imports:
    - { resource: parameters.yml }
    - { resource: assets.php }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    days_to_keep_logs: 7
    user_forum_creation_interval: 1 day
    uuid_regex:      '[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}'
    wiki_page_regex: '[A-Za-z][A-Za-z0-9_-]*(/[A-Za-z][A-Za-z0-9_-]*)*'
    locale: en

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%env(SECRET)%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: false
    assets:
        json_manifest_path: "%kernel.root_dir%/../web/build/manifest.json"
    php_errors:
        log: true
    cache:           { app: cache.system }
    web_link:        { enabled: true }


# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form_themes:
        - "RadditAppBundle:forms:standard_form_theme.html.twig"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_pgsql
        url:      "%env(DATABASE_URL)%"
        charset:  UTF8
        types:
            inet: Doctrine\DBAL\PostgresTypes\InetType
            uuid: Ramsey\Uuid\Doctrine\UuidType
        mapping_types:
            inet: inet
            uuid: uuid
    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%env(MAILER_TRANSPORT)%"
    host:      "%env(MAILER_HOST)%"
    username:  "%env(MAILER_USER)%"
    password:  "%env(MAILER_PASSWORD)%"
    spool:     { type: memory }

doctrine_migrations:
    dir_name: "%kernel.root_dir%/../src/AppBundle/DoctrineMigrations"
    namespace: Raddit\AppBundle\DoctrineMigrations
    name: Raddit migrations

# todo: use flysystem for thumbnails too
liip_imagine:
    loaders:
        submission_images:
            flysystem:
                filesystem_service: oneup_flysystem.submission_images_filesystem
    filter_sets:
        submission_thumbnail:
            data_loader: submission_images
            filters:
                thumbnail:
                    size: [200, 200]
                    mode: outbound
            quality: 70

oneup_flysystem:
    adapters:
        submission_images:
            local:
                directory: "%kernel.root_dir%/../web/submission_images"
    filesystems:
        submission_images: { adapter: submission_images }

scheb_two_factor:
    email:
        # todo - disable when env(NO_REPLY_ADDRESS) is null
        enabled: true
        sender_email: "%env(NO_REPLY_ADDRESS)%"
        sender_name: "%env(SITE_NAME)%"
        template: RadditAppBundle::security_2fa_form.html.twig
        digits: 8
    exclude_pattern: ^/((build|bundles|fonts|media|submission_images)/|(favicon\.ico|apple-touch-icon-precomposed\.png)$)
    security_tokens:
        - Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken
        - Symfony\Component\Security\Core\Authentication\Token\RememberMeToken

eo_honeypot:
    redirect: { enabled: false }
